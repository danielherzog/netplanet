﻿using Microsoft.AspNet.Identity.EntityFramework;
using netplanet.dal.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace netplanet.dal
{
    public class NetplanetContext : IdentityDbContext<ApplicationUser>
    {
        public virtual IDbSet<Agreement> Agreements { get; set; }
        public virtual IDbSet<AgreementArticle> AgreementArticles { get; set; }
        public virtual IDbSet<Commission> Commissions { get; set; }
        public virtual IDbSet<Invoice> Invoices { get; set; }
        public virtual IDbSet<Position> InvoicePositions { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<AgreementArticle>()
                .Map<SingularArticle>(m => m.Requires("ArticleType").HasValue("Singular"))
                .Map<RecurringArticle>(m => m.Requires("ArticleType").HasValue("Recurring"))
                .Map<VolumeArticle>(m => m.Requires("ArticleType").HasValue("Volume"));

            modelBuilder.Entity<Commission>()
                .Map<PercentCommission>(m => m.Requires("CommissionType").HasValue("Percent"))
                .Map<AbsoluteCommission>(m => m.Requires("CommissionType").HasValue("Absolute"));

            
        }
    }
}
