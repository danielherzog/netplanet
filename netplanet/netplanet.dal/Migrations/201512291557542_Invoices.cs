namespace netplanet.dal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Invoices : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Positions",
                c => new
                    {
                        PosId = c.Int(nullable: false, identity: true),
                        InvoiceId = c.Int(nullable: false),
                        Amount = c.Int(nullable: false),
                        Price = c.Decimal(nullable: false, precision: 18, scale: 2),
                        CommissionId = c.Int(),
                        CreateUser = c.String(nullable: false, maxLength: 50),
                        CreateDate = c.DateTime(nullable: false),
                        UpdateUser = c.String(maxLength: 50),
                        UpdateDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.PosId)
                .ForeignKey("dbo.Commissions", t => t.CommissionId)
                .ForeignKey("dbo.Invoices", t => t.InvoiceId, cascadeDelete: true)
                .Index(t => t.InvoiceId)
                .Index(t => t.CommissionId);
            
            CreateTable(
                "dbo.Invoices",
                c => new
                    {
                        InvoiceId = c.Int(nullable: false, identity: true),
                        CustomerId = c.Int(nullable: false),
                        Type = c.Int(nullable: false),
                        Group = c.String(maxLength: 50),
                        Description = c.String(nullable: false, maxLength: 200),
                        BillingDate = c.DateTime(nullable: false, storeType: "smalldatetime"),
                        TransferDate = c.DateTime(),
                        CreateUser = c.String(nullable: false, maxLength: 50),
                        CreateDate = c.DateTime(nullable: false),
                        UpdateUser = c.String(maxLength: 50),
                        UpdateDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.InvoiceId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Positions", "InvoiceId", "dbo.Invoices");
            DropForeignKey("dbo.Positions", "CommissionId", "dbo.Commissions");
            DropIndex("dbo.Positions", new[] { "CommissionId" });
            DropIndex("dbo.Positions", new[] { "InvoiceId" });
            DropTable("dbo.Invoices");
            DropTable("dbo.Positions");
        }
    }
}
