namespace netplanet.dal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CommissionTypes : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Commissions",
                c => new
                    {
                        CommissionId = c.Int(nullable: false, identity: true),
                        Active = c.Boolean(nullable: false),
                        Agent = c.Int(nullable: false),
                        CreateUser = c.String(nullable: false, maxLength: 50),
                        CreateDate = c.DateTime(nullable: false),
                        UpdateUser = c.String(maxLength: 50),
                        UpdateDate = c.DateTime(),
                        Value = c.Decimal(precision: 18, scale: 2),
                        Percent = c.Int(),
                        CommissionType = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.CommissionId);
            
            AddColumn("dbo.AgreementArticles", "CommissionId", c => c.Int());
            CreateIndex("dbo.AgreementArticles", "CommissionId");
            AddForeignKey("dbo.AgreementArticles", "CommissionId", "dbo.Commissions", "CommissionId");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AgreementArticles", "CommissionId", "dbo.Commissions");
            DropIndex("dbo.AgreementArticles", new[] { "CommissionId" });
            DropColumn("dbo.AgreementArticles", "CommissionId");
            DropTable("dbo.Commissions");
        }
    }
}
