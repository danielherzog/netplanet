namespace netplanet.dal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InvoicesDescription : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Positions", "Description", c => c.String(nullable: false, maxLength: 500));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Positions", "Description");
        }
    }
}
