namespace netplanet.dal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ArticleTypes : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AgreementArticles", "Paid", c => c.DateTime());
            AddColumn("dbo.AgreementArticles", "ArticleType", c => c.String(nullable: false, maxLength: 128));
        }
        
        public override void Down()
        {
            DropColumn("dbo.AgreementArticles", "ArticleType");
            DropColumn("dbo.AgreementArticles", "Paid");
        }
    }
}
