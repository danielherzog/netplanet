// <auto-generated />
namespace netplanet.dal.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class ArticleTypes : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(ArticleTypes));
        
        string IMigrationMetadata.Id
        {
            get { return "201512291446008_ArticleTypes"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
