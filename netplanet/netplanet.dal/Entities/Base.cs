﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace netplanet.dal.Entities
{
    public class Base
    {
        [Required]
        [StringLength(50)]
        public string CreateUser { get; set; }

        [Required]
        public DateTime CreateDate { get; set; }
        
        [StringLength(50)]
        public string UpdateUser { get; set; }
        
        public DateTime? UpdateDate { get; set; }
    }
}
