﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace netplanet.dal.Entities
{
    public class Agreement : Base
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int PosId { get; set; }

        [Required]
        public int CustomerId { get; set; }

        [Required]
        public bool Active { get; set; }

        [StringLength(100)]
        [Required]
        public string Name { get; set; }

        [StringLength(500)]
        public string Description { get; set; }               

        [StringLength(500)]
        public string Note { get; set; }

        [Required]
        [Column(TypeName = "smalldatetime")]
        public DateTime SetupDate { get; set; }

        [Required]
        [Column(TypeName ="smalldatetime")]
        public DateTime InvoiceDate { get; set; }

        [Required]
        [Column(TypeName = "smalldatetime")]
        public DateTime ContractDate { get; set; }

        [Required]
        [Column(TypeName = "smalldatetime")]
        public DateTime BillingDate { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? LastBillingDate { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? NoticeDate { get; set; }

        [Required]
        public int BillingInterval { get; set; }

        [Required]
        public bool PrePayment { get; set; }

        [Required]
        public int Term { get; set; }

        [Required]
        public int NoticePeriod { get; set; }

        public int? LinkedPosId { get; set; }

        public virtual ICollection<AgreementArticle> Articles { get; set; }

        [ForeignKey("LinkedPosId")]
        public virtual Agreement LinkedAgreement { get; set; }

        public virtual ICollection<Agreement> LinkedAgreements { get; set; } 
        
    }
}
