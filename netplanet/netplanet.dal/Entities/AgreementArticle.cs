﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace netplanet.dal.Entities
{
    public abstract class AgreementArticle : Base
    {
        [Key]
        [Column(Order = 1)]
        public int PosId { get; set; }

        [Key]
        [Column(Order = 2)]
        public int ArticleId { get; set; }

        [Required]
        public int Amount { get; set; }

        public decimal Price { get; set; }

        [ForeignKey("PosId")]
        public virtual Agreement Agreement { get; set; }

        public int? CommissionId { get; set; }

        [ForeignKey("CommissionId")]
        public virtual Commission Commission { get; set; }

    }

    public class SingularArticle : AgreementArticle
    {
        public DateTime? Paid { get; set; }
    }

    public class RecurringArticle : AgreementArticle
    {

    }

    public class VolumeArticle : AgreementArticle
    {

    }
}
