﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace netplanet.dal.Entities
{
    public abstract class Commission : Base
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int CommissionId { get; set; }

        [Required]
        public bool Active { get; set; }

        [Required]
        public int Agent { get; set; }
    }

    public class PercentCommission : Commission
    {
        [Required]
        public int Percent { get; set; }
    }

    public class AbsoluteCommission : Commission
    {
        public decimal Value { get; set; }
    }
}
