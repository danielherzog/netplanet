using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace netplanet.dal.Entities
{
    public class Position : Base
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int PosId { get; set; }

        [Required]
        public int InvoiceId { get; set; }

        [Required]
        [StringLength(500)]
        public string Description { get; set; }

        [Required]
        public int Amount { get; set; }

        [Required]
        public decimal Price { get; set; }

        [ForeignKey("InvoiceId")]
        public virtual Invoice Invoice { get; set; }

        public int? CommissionId { get; set; }

        [ForeignKey("CommissionId")]
        public virtual Commission Commission { get; set; }
    }
}