﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace netplanet.dal.Entities
{
    public class ApplicationUser : IdentityUser
    {
        [StringLength(100)]
        public string Firstname { get; set; }

        [StringLength(100)]
        public string Lastname { get; set; }

        public DateTime? LastLogin { get; set; }

        [Required]
        public bool Active { get; set; }
    }
}
