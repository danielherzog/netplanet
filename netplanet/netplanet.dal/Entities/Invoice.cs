﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace netplanet.dal.Entities
{
    public enum Type
    {
        All = 0,
        Agreement = 1
    }

    public class Invoice : Base
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int InvoiceId { get; set; }

        [Required]
        public int CustomerId { get; set; }

        [Required]
        public Type Type { get; set; }

        [StringLength(50)]
        public string Group { get; set; }

        [Required]
        [StringLength(200)]
        public string Description { get; set; }

        [Required]
        [Column(TypeName = "smalldatetime")]
        public DateTime BillingDate { get; set; }

        public DateTime? TransferDate { get; set; }

        public virtual ICollection<Position> Positions { get; set; }
             
    }
}
