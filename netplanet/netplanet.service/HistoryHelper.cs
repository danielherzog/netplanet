﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using netplanet.dal.Entities;

namespace netplanet.service
{
    public static class HistoryHelper
    {
        public static void Create(this Base entity)
        {
            entity.CreateDate = DateTime.Now;

            var identity = Thread.CurrentPrincipal.Identity as ClaimsIdentity;
            entity.CreateUser = identity != null ? identity.Claims.First(x => x.Type.Equals("sub")).Value : "unknown";
        }

        public static void Update(this Base entity)
        {
            entity.UpdateDate = DateTime.Now;
            var identity = Thread.CurrentPrincipal.Identity as ClaimsIdentity;
            entity.UpdateUser = identity != null ? identity.Claims.First(x => x.Type.Equals("sub")).Value : "unknown";
        }
    }
}
