﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using netplanet.dal.Entities;

namespace netplanet.service.Interfaces
{
    public interface IAgreementService
    {
        Task<IEnumerable<Agreement>> All();
        Task<IEnumerable<Agreement>> Active();
        Task<IEnumerable<Agreement>> Search(string term, bool onlyActive = true);
        Task<Agreement> Get(int id);
        Task Add(Agreement agreement);
        Task Update(Agreement agreement);
        Task Delete(int id);
        Task AddLink(int id, int linkid);
        Task DeleteLink(int id);
        Task<int> Count(bool active);
        Task<decimal> Sum();
    }
}
