﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using netplanet.dal.Entities;

namespace netplanet.service.Interfaces
{
    public interface IArticleService
    {
        Task<IEnumerable<AgreementArticle>> Get(int posId);
        Task<AgreementArticle> Get(int posId, int articleId);
        Task Add(int posId, AgreementArticle model);
        Task Update(AgreementArticle model);
        Task Delete(int posId, int articleId);
    }
}
