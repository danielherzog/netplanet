﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using mbit.common.dal;
using mbit.common.dal.Repositories;
using mbit.common.logging;
using netplanet.dal.Entities;
using netplanet.service.Interfaces;

namespace netplanet.service
{
    public class ArticleService : IArticleService
    {
        private static readonly ImcLogger _logger = mcLogFactory.GetCurrentLogger();
        private readonly IUnitOfWork _unitOfWork = null;
        private readonly IRepository<AgreementArticle> _agreementArticleRepo = null;
        

        public ArticleService(IUnitOfWork unitOfWork, IRepository<AgreementArticle> agreementArticleRepo)
        {
            _unitOfWork = unitOfWork;
            _agreementArticleRepo = agreementArticleRepo;
        }

        public async Task<IEnumerable<AgreementArticle>> Get(int posId)
        {
            try
            {
                return await _agreementArticleRepo.GetAsync(x => x.PosId == posId);
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Error while getting articles for agreement {0}", posId);
                throw;
            }
        }

        public async Task<AgreementArticle> Get(int posId, int articleId)
        {
            try
            {
                return await _agreementArticleRepo.GetSingleAsync(x => x.PosId == posId && x.ArticleId == articleId);
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Error while getting articles for agreement {0}", posId);
                throw;
            }
        }

        public async Task Add(int posId, AgreementArticle model)
        {
            try
            {
                model.PosId = posId;
                model.Create();
                _agreementArticleRepo.Add(model);
                await _unitOfWork.SaveAsync();
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Error while adding article to agreement {0}", posId);
                throw;
            }
        }

        public async Task Update(AgreementArticle model)
        {
            try
            {
                model.Update();
                _agreementArticleRepo.Update(model);
                await _unitOfWork.SaveAsync();
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Error while updating article {0} from agreement {1}", model.ArticleId, model.PosId);
                throw;
            }
        }

        public async Task Delete(int posId, int articleId)
        {
            try
            {
                _agreementArticleRepo.Delete(x => x.PosId == posId && x.ArticleId == articleId);
                await _unitOfWork.SaveAsync();
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Error while delete article {0} from agreement {1}", articleId, posId);
                throw;
            }
        }
    }
}
