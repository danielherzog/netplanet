﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using mbit.common.dal;
using mbit.common.dal.Repositories;
using mbit.common.logging;
using netplanet.dal.Entities;
using netplanet.service.Interfaces;

namespace netplanet.service
{
    public class AgreementService : IAgreementService
    {
        private static readonly ImcLogger _logger = mcLogFactory.GetCurrentLogger();
        private readonly IUnitOfWork _unitOfWork = null;
        private readonly IRepository<Agreement> _agreementRepo = null;
        private readonly IRepository<AgreementArticle> _agreementArticleRepo = null;
        private readonly IRepository<Invoice> _invoiceRepo = null;
        private readonly IRepository<Position> _posRepo = null;

        public AgreementService(IUnitOfWork unitOfWork, IRepository<Agreement> agreementRepo, IRepository<AgreementArticle> agreementArticleRepo, IRepository<Invoice> invoiceRepo, IRepository<Position> posRepo)
        {
            _unitOfWork = unitOfWork;
            _agreementRepo = agreementRepo;
            _agreementArticleRepo = agreementArticleRepo;
            _invoiceRepo = invoiceRepo;
            _posRepo = posRepo;
        }

        public async Task<IEnumerable<Agreement>> All()
        {
            try
            {
                return await _agreementRepo.GetAllAsync();
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Error while getting all agreements");
                throw;
            }
        }

        public async Task<IEnumerable<Agreement>> Active()
        {
            try
            {
                return await _agreementRepo.GetAsync(x => x.Active);
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Error while getting all agreements");
                throw;
            }
        }

        public async Task<decimal> Sum()
        {
            var services = await _agreementRepo.GetAsync(x => x.Active);
            var sum = services.Select(x => x.Articles.Sum(y => y.Amount*y.Price)).Sum(x => x);
            return sum;
        }

        public async Task<IEnumerable<Agreement>> Search(string term, bool onlyActive = true)
        {
            try
            {
                var terms = term.Split(new string[] { " ", ",", ";" }, StringSplitOptions.RemoveEmptyEntries);

                if (onlyActive)
                {
                    return await _agreementRepo.GetAsync(x => x.Active &&
                    terms.Any(y =>
                        x.CustomerId.ToString().Contains(y) ||
                        x.Name.Contains(y) ||
                        x.Description.Contains(y)
                        )
                    );
                }

                return await _agreementRepo.GetAsync(x =>
                    terms.Any(y =>
                        x.CustomerId.ToString().Contains(y) ||
                        x.Name.Contains(y) ||
                        x.Description.Contains(y)
                        )
                    );
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Error while searching agreements with term {0}", term);
                throw;
            }
        }

        public async Task<Agreement> Get(int id)
        {
            try
            {
                return await _agreementRepo.GetByIdAsync(id);
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Error while getting agreement with id {0}", id);
                throw;
            }
        }

        public async Task Add(Agreement agreement)
        {
            try
            {
                agreement.BillingDate = agreement.ContractDate;
                agreement.Create();
                _agreementRepo.Add(agreement);
                await _unitOfWork.SaveAsync();
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Error while adding agreement");
                throw;
            }
        }

        public async Task Update(Agreement agreement)
        {
            try
            {
                agreement.Update();
                _agreementRepo.Update(agreement);
                await _unitOfWork.SaveAsync();
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Error while updating agreement with id {0}", agreement.PosId);
                throw;
            }
        }

        public async Task Delete(int id)
        {
            try
            {
                _agreementRepo.Delete(id);
                await _unitOfWork.SaveAsync();
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Error while deleting agreement with id {0}", id);
                throw;
            }
        }

        public async Task AddLink(int id, int linkid)
        {
            try
            {
                var agreement = await _agreementRepo.GetByIdAsync(id);
                agreement.LinkedPosId = linkid;
                agreement.Update();
                _agreementRepo.Update(agreement);
                await _unitOfWork.SaveAsync();
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Error while adding link from agreement {0} to agreement {1}", id, linkid);
                throw;
            }
        }

        public async Task DeleteLink(int id)
        {
            try
            {
                var agreement = await _agreementRepo.GetByIdAsync(id);
                agreement.LinkedPosId = null;
                agreement.Update();
                _agreementRepo.Update(agreement);
                await _unitOfWork.SaveAsync();
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Error while removing link from agreement {0}", id);
                throw;
            }
        }

        public async Task<int> Count(bool active)
        {
            try
            {
                return await _agreementRepo.CountAsync(x => x.Active == active);
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Error while counting agreements");
                throw;
            }
        }
    }
}
