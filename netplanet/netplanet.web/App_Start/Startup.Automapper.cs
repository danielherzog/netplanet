﻿using Autofac;
using Autofac.Integration.WebApi;
using AutoMapper;
using mbit.common.dal;
using mbit.common.dal.Repositories;
using Microsoft.Owin.Security.OAuth;
using netplanet.dal;
using netplanet.dal.Entities;
using netplanet.web.Models;
using netplanet.web.Provider;
using Owin;
using System;
using System.Data.Entity;
using System.Reflection;
using System.Web.Http;
using System.Web.Mvc;
using netplanet.web.Models.Commission;

namespace netplanet.web
{
    public partial class Startup
    {        
        public void ConfigureAutomapper(IAppBuilder app)
        {
            Mapper.CreateMap<Agreement, AgreementModel>();

            Mapper.CreateMap<Agreement, AgreementListModel>();

            Mapper.CreateMap<AgreementModel, Agreement>()
                .ForMember(dst => dst.PosId, map => map.Ignore())
                .ForMember(dst => dst.LinkedAgreement, map => map.Ignore())
                .ForMember(dst => dst.LinkedAgreements, map => map.Ignore());

            Mapper.CreateMap<AgreementArticle, ArticleListModel>();

            Mapper.CreateMap<AgreementArticle, ArticleModel>();

            Mapper.CreateMap<Commission, CommissionModel>();

            Mapper.CreateMap<PercentCommission, PercentCommissionModel>();

            Mapper.CreateMap<AbsoluteCommission, AbsoluteCommissionModel>();

            Mapper.CreateMap<ApplicationUser, UserListModel>();

            Mapper.CreateMap<ApplicationUser, UserModel>();

            Mapper.CreateMap<UserModel, ApplicationUser>()
                .ForMember(dst => dst.Id, map => map.Ignore());

            Mapper.CreateMap<UserAddModel, ApplicationUser>()
                .IncludeBase<UserModel, ApplicationUser>();
        }
    }
}
