﻿using Autofac;
using Autofac.Integration.WebApi;
using mbit.common.dal;
using mbit.common.dal.Repositories;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security.OAuth;
using netplanet.dal;
using netplanet.dal.Entities;
using netplanet.web.Provider;
using Owin;
using System;
using System.Data.Entity;
using System.Reflection;
using System.Web.Http;
using System.Web.Mvc;
using netplanet.service;
using netplanet.service.Interfaces;

namespace netplanet.web
{
    public partial class Startup
    {        
        public IContainer ConfigureDependency(IAppBuilder app)
        {
            var builder = new ContainerBuilder();
            var config = GlobalConfiguration.Configuration;

            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());
            this.RegisterTypes(builder);

            var container = builder.Build();
            //DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
            config.DependencyResolver = new AutofacWebApiDependencyResolver(container);

            return container;
        }

        private void RegisterTypes(ContainerBuilder builder)
        {
            builder.RegisterType<NetplanetContext>().As<DbContext>().InstancePerLifetimeScope();
            builder.RegisterType<EfUnitOfWork>().As<IUnitOfWork>().As<IContextProvider<DbContext>>().InstancePerLifetimeScope();
            builder.RegisterGeneric(typeof(EfRepositoryBase<>)).As(typeof(IRepository<>)).InstancePerLifetimeScope();

            builder.RegisterType<AgreementService>().As<IAgreementService>().InstancePerLifetimeScope();
            builder.RegisterType<ArticleService>().As<IArticleService>().InstancePerLifetimeScope();

            builder.RegisterType<UserStore<ApplicationUser>>().As<IUserStore<ApplicationUser>>().InstancePerLifetimeScope();
            builder.RegisterType<UserManager<ApplicationUser>>().AsSelf().InstancePerLifetimeScope();

            builder.RegisterType<ApiAuthorizationServerProvider>().AsSelf().SingleInstance();
        }
    }
}
