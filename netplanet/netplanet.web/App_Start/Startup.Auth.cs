﻿using Autofac;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security.OAuth;
using netplanet.dal.Entities;
using netplanet.web.Provider;
using Owin;
using System;
using System.Web.Mvc;

namespace netplanet.web
{
    public partial class Startup
    {        
        public void ConfigureAuth(IAppBuilder app, IContainer container)
        {
            var serverOptions = new OAuthAuthorizationServerOptions()
            {
                AllowInsecureHttp = true,
                TokenEndpointPath = new Microsoft.Owin.PathString("/token"),
                AccessTokenExpireTimeSpan = TimeSpan.FromHours(1),
                Provider = container.Resolve<ApiAuthorizationServerProvider>()
            };

            app.UseOAuthAuthorizationServer(serverOptions);
            app.UseOAuthBearerAuthentication(new OAuthBearerAuthenticationOptions());
        }
    }
}
