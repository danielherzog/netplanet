﻿using mbit.common.dal;
using mbit.common.dal.Repositories;
using netplanet.dal.Entities;
using netplanet.web.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace netplanet.web.Controllers
{
    [RoutePrefix("api/Customer")]
    [Authorize]
    public class CustomerController : ApiController
    {

        [Route("All")]
        public async Task<IHttpActionResult> Get()
        {
            try
            {
                var list = new ArrayList
                {
                    new { id=20000, name="Netplanet GmbH" },
                    new { id=20001, name="qrmore GmbH" },
                    new { id=20002, name="MBIT Solutions GmbH" }
                };

                return Ok(list);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }
    }
}
