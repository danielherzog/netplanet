﻿using mbit.common.dal;
using mbit.common.dal.Repositories;
using netplanet.dal.Entities;
using netplanet.web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web.Http;
using netplanet.service.Interfaces;
using Type = netplanet.dal.Entities.Type;

namespace netplanet.web.Controllers
{
    [RoutePrefix("api/Agreement")]
    [Authorize]
    public class AgreementController : ApiController
    {
        private readonly IAgreementService _agreementService;
        private readonly IArticleService _articleService;

        public AgreementController(IAgreementService agreementService, IArticleService articleService)
        {
            _agreementService = agreementService;
            _articleService = articleService;
        }

        [Route("")]
        [HttpGet]
        public async Task<IHttpActionResult> Get(string term = null, bool inactive = false)
        {
            try
            {
                IEnumerable<Agreement> list = null;

                if (string.IsNullOrWhiteSpace(term))
                {
                    if (inactive)
                    {
                        list = await _agreementService.All();
                    }
                    else
                    {
                        list = await _agreementService.Active();
                    }
                }
                else
                {
                    list = await _agreementService.Search(term, !inactive);
                }
                return Ok(list.Select(x => new AgreementListModel(x)));
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [Route("Count")]
        [HttpGet]
        public async Task<IHttpActionResult> Count()
        {
            try
            {
                var count = await _agreementService.Count(true);
                return Ok(count);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [Route("Sum")]
        [HttpGet]
        public async Task<IHttpActionResult> Sum()
        {
            try
            {
                var sum = await _agreementService.Sum();
                return Ok(sum);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [Route("Linked")]
        [HttpGet]
        public async Task<IHttpActionResult> GetLinked()
        {
            try
            {
                var list = await _agreementService.All();
                return Ok(list.Select(x => new AgreementListModel(x)));
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [Route("{id:int}")]
        [HttpGet]
        public async Task<IHttpActionResult> GetById(int id)
        {
            try
            {
                var model = await _agreementService.Get(id);
                return Ok(new AgreementModel(model));
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [Route("")]
        [HttpPost]
        public async Task<IHttpActionResult> Add(AgreementModel model)
        {
            try
            {
                var agreement = new Agreement();
                model.BindTo(agreement);
                await _agreementService.Add(agreement);
                return Ok();
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [Route("{id:int}")]
        [HttpPut]
        public async Task<IHttpActionResult> Update(int id, AgreementModel model)
        {
            try
            {
                var agreement = await _agreementService.Get(id);
                model.BindTo(agreement);
                await _agreementService.Update(agreement);
                return Ok();
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [Route("{id:int}")]
        [HttpDelete]
        public async Task<IHttpActionResult> Delete(int id)
        {
            try
            {
                await _agreementService.Delete(id);
                return Ok();
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [Route("{id:int}/Link/{linkid:int}")]
        [HttpPut]
        public async Task<IHttpActionResult> AddLink(int id, int linkid)
        {
            try
            {
                await _agreementService.AddLink(id, linkid);
                return Ok();
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);

            }
        }

        [Route("{id:int}/Link")]
        [HttpDelete]
        public async Task<IHttpActionResult> DeleteLink(int id)
        {
            try
            {
                await _agreementService.DeleteLink(id);
                return Ok();
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [Route("{id:int}/Articles")]
        [HttpGet]
        public async Task<IHttpActionResult> GetArticles(int id)
        {
            try
            {
                var articles = await _articleService.Get(id);
                return Ok(articles.Select(x => new ArticleListModel(x)));
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [Route("{id:int}/Articles/{articleid:int}")]
        [HttpGet]
        public async Task<IHttpActionResult> GetArticle(int id, int articleid)
        {
            try
            {
                var article = await _articleService.Get(id, articleid);
                return Ok(new ArticleModel(article));
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [Route("{id:int}/Articles")]
        [HttpPost]
        public async Task<IHttpActionResult> AddArticle(int id, ArticleModel model)
        {
            try
            {
                var article = model.Get();
                await _articleService.Add(id, article);
                return Ok();
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [Route("{id:int}/Articles/{articleid:int}")]
        [HttpPut]
        public async Task<IHttpActionResult> UpdateArticle(int id, int articleid, ArticleModel model)
        {
            try
            {
                var article = await _articleService.Get(id, articleid);
                model.BindTo(article);
                await _articleService.Update(article);
                return Ok();
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [Route("{id:int}/Articles/{articleid:int}")]
        [HttpDelete]
        public async Task<IHttpActionResult> DeleteArticle(int id, int articleid)
        {
            try
            {
                await _articleService.Delete(id, articleid);
                return Ok();
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        //[Route("Billing")]
        //[HttpGet]
        //[AllowAnonymous]
        //public async Task<IHttpActionResult> Billing()
        //{
        //    DateTime? billingDate = DateTime.Parse("2016-01-01");

        //    if (!billingDate.HasValue)
        //    {
        //        return BadRequest("Kein Rechnungsdatum");
        //    }

        //    var agreements = await _agreementRepo.GetAsync(x => x.Active && x.BillingDate == billingDate.Value);

        //    foreach (var service in agreements)
        //    {
        //        var invoice = this.BuildInvoice(billingDate.Value, Type.All, service.CustomerId);

        //        var articles = await _agreementArticleRepo.GetAsync(x => x.PosId == service.PosId);

        //        foreach (var article in articles.OfType<SingularArticle>().Where(x => x.Paid == null))
        //        {
        //            var pos = new Position
        //            {
        //                Amount = article.Amount,
        //                Price = article.Price,
        //                Invoice = invoice,
        //                CreateDate = DateTime.Now,
        //                CreateUser = "mbit"
        //            };

        //            _posRepo.Add(pos);

        //        }

        //        foreach (var article in articles.OfType<RecurringArticle>())
        //        {
        //            var pos = new Position
        //            {
        //                Amount = article.Amount,
        //                Price = article.Price,
        //                Invoice = invoice,
        //                Commission = article.Commission,
        //                CreateDate = DateTime.Now,
        //                CreateUser = "mbit"
        //            };

        //            _posRepo.Add(pos);

        //        }

        //        await _unitOfWork.SaveAsync();
        //    }
        //    return Ok();
        //}

        //private Invoice BuildInvoice(DateTime billingDate, Type type, int customerId, string group = null)
        //{
        //    var invoice = _invoiceRepo.GetSingle(x => x.Type == type && x.CustomerId == customerId && x.Group.Equals(group) && x.TransferDate == null);

        //    if (invoice == null)
        //    {
        //        invoice = new Invoice()
        //        {
        //            Type = type,
        //            BillingDate = billingDate,
        //            CustomerId = customerId,
        //            Description = "test",
        //            CreateDate = DateTime.Now,
        //            CreateUser = "Abrechnung vom " + billingDate.ToShortDateString()
        //        };

        //        _invoiceRepo.Add(invoice);
        //    }

        //    return invoice;
        //}
    }
}
