﻿using mbit.common.dal;
using mbit.common.dal.Repositories;
using netplanet.dal.Entities;
using netplanet.web.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace netplanet.web.Controllers
{
    [RoutePrefix("api/Article")]
    [Authorize]
    public class ArticleController : ApiController
    {

        [Route("All")]
        public async Task<IHttpActionResult> Get()
        {
            try
            {
                var list = new ArrayList
                {
                    new { id=4890220,  nr="ART0000231", name="HOUSING" },
                    new { id=4890221,  nr="ART0000248", name="DOMAIN" },
                    new { id=4890222,  nr="ART0000393", name="SIPTRUNK" },
                    new { id=4890223,  nr="ART0000243", name="WEBSPACE" },
                    new { id=4890224,  nr="ART0000406", name="SIPBUNDLE" },
                    new { id=4890225,  nr="ART0000235", name="RIPE" },
                    new { id=4890226,  nr="ART0000237", name="DSLINFRA" },
                    new { id=4890227,  nr="ART0000411", name="SIPTRUNK" }
                };

                return Ok(list);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }
    }
}
