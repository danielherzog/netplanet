﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Microsoft.AspNet.Identity;
using netplanet.dal.Entities;
using netplanet.web.Models;

namespace netplanet.web.Controllers
{
    [Authorize]
    [RoutePrefix("api/Users")]
    public class UserController : ApiController
    {
        private readonly UserManager<ApplicationUser> _userManager;

        public UserController(UserManager<ApplicationUser> userManager)
        {
            _userManager = userManager;
        }

        [Route("")]
        [HttpGet]
        public IHttpActionResult All()
        {
            try
            {
                var users = _userManager.Users.ToList();
                return Ok(users.Select(x => new UserListModel(x)));
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [Route("{id}")]
        [HttpGet]
        public async Task<IHttpActionResult> Get(string id)
        {
            try
            {
                var user = await _userManager.FindByIdAsync(id);
                return Ok(new UserModel(user));
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [Route("")]
        [HttpPost]
        public async Task<IHttpActionResult> Add(UserAddModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }

                var user = new ApplicationUser();
                model.BindTo(user);
                await _userManager.CreateAsync(user, model.Password);
                return Ok();
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [Route("{id}")]
        [HttpPut]
        public async Task<IHttpActionResult> Update(string id, UserModel model)
        {
            try
            {
                var user = await _userManager.FindByIdAsync(id);
                model.BindTo(user);
                await _userManager.UpdateAsync(user);
                return Ok();
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [Route("{id}")]
        [HttpDelete]
        public async Task<IHttpActionResult> Delete(string id)
        {
            try
            {
                var user = await _userManager.FindByIdAsync(id);
                await _userManager.DeleteAsync(user);
                return Ok();
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }
    }
}
