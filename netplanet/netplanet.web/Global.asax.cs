﻿using Microsoft.AspNet.Identity;
using netplanet.dal;
using netplanet.dal.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace netplanet.web
{
    public class MyInitializer : DropCreateDatabaseAlways<NetplanetContext>
    {
        protected override void Seed(NetplanetContext context)
        {
            var userManager = DependencyResolver.Current.GetService<UserManager<ApplicationUser>>();

            var user = new ApplicationUser();
            user.UserName = "daniel";

            userManager.Create(user, "123456");
        }
    }

    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            
        }
    }
}
