﻿using netplanet.dal.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace netplanet.web.Helpers
{
    public class BillingDateHelper
    {
        public static DateTime NextNoticeDate(Agreement model)
        {
            var next = model.ContractDate.AddMonths(model.Term - model.NoticePeriod).AddDays(-1);

            while(next < DateTime.Now)
            {
                next = next.AddMonths(model.Term);
            }

            return next;
        }
    }
}
