﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using netplanet.dal.Entities;

namespace netplanet.web.Models
{
    [DataContract]
    public class UserModel
    {
        [DataMember(Name="id")]
        public string Id { get; set; }

        [DataMember(Name = "username")]
        public string UserName { get; set; }

        [DataMember(Name = "email")]
        public string Email { get; set; }

        [DataMember(Name = "firstname")]
        public string Firstname { get; set; }

        [DataMember(Name = "lastname")]
        public string Lastname { get; set; }

        [DataMember(Name = "lastlogin")]
        public DateTime? LastLogin { get; set; }

        public UserModel()
        {
            
        }

        public UserModel(ApplicationUser model)
        {
            Mapper.Map(model, this);
        }

        public void BindTo(ApplicationUser model)
        {
            Mapper.Map(this, model);
        }
    }
}
