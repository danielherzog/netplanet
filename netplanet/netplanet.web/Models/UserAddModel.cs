﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using netplanet.dal.Entities;

namespace netplanet.web.Models
{
    [DataContract]
    public class UserAddModel : UserModel
    {
        [DataMember(Name="password")]
        public string Password { get; set; }

        [DataMember(Name = "passwordConfirm")]
        [Compare("Password")]
        public string PasswordConfirm { get; set; }


        public UserAddModel()
        {
            
        }

        public UserAddModel(ApplicationUser model)
        {
            Mapper.Map(model, this);
        }
    }
}
