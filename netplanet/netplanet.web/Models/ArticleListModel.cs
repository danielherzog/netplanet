﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using netplanet.dal.Entities;

namespace netplanet.web.Models
{
    [DataContract]
    public class ArticleListModel
    {
        [DataMember(Name = "id")]
        public int ArticleId { get; set; }

        [DataMember(Name = "amount")]
        public int Amount { get; set; }

        [DataMember(Name = "price")]
        public decimal Price { get; set; }

        [DataMember(Name = "type")]
        public string Type  { get; set; }

        public ArticleListModel(AgreementArticle model)
        {
            Mapper.Map(model, this);

            if (model is SingularArticle)
            {
                this.Type = "Einmalig";
            }
            else if (model is RecurringArticle)
            {
                this.Type = "Wiederkehrend";
            }
            else
            {
                this.Type = "Volume";
            }

        }
    }
}
