﻿using AutoMapper;
using netplanet.dal.Entities;
using netplanet.web.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace netplanet.web.Models
{
    [DataContract]
    public class AgreementModel
    {
        [DataMember(Name = "id")]
        public int PosId { get; set; }

        [DataMember(Name = "customer")]
        public int CustomerId { get; set; }

        [DataMember(Name = "name")]
        public string Name { get; set; }

        [DataMember(Name = "description")]
        public string Description { get; set; }

        [DataMember(Name = "setupdate")]
        public DateTime SetupDate { get; set; }

        [DataMember(Name = "contractdate")]
        public DateTime ContractDate { get; set; }

        [DataMember(Name = "invoicedate")]
        public DateTime InvoiceDate { get; set; }

        [DataMember(Name = "nextbillingdate")]
        public DateTime BillingDate { get; set; }

        [DataMember(Name = "lastbillingdate")]
        public DateTime? LastBillingDate { get; set; }

        [DataMember(Name = "noticedate")]
        public DateTime? NoticeDate { get; set; }

        [DataMember(Name = "interval")]
        public int BillingInterval { get; set; }

        [DataMember(Name = "term")]
        public int Term { get; set; }

        [DataMember(Name = "noticeperiod")]
        public int NoticePeriod { get; set; }

        [DataMember(Name = "nextnoticedate")]
        public DateTime NextNoticeDate { get; set; }

        [DataMember(Name = "prepayment")]
        public bool PrePayment { get; set; }

        [DataMember(Name = "active")]
        public bool Active { get; set; }

        [DataMember(Name = "agreements")]
        public IEnumerable<AgreementListModel> LinkedAgreements { get; set; }

        [DataMember(Name = "linked")]
        public AgreementModel Linked { get; set; }

        public AgreementModel()
        {

        }

        public AgreementModel(Agreement model)
        {
            Mapper.Map(model, this);
            this.LinkedAgreements = model.LinkedAgreements.Select(x => new AgreementListModel(x));

            if(model.LinkedAgreement != null)
            { 
                this.Linked = new AgreementModel(model.LinkedAgreement);
            }
            this.NextNoticeDate = BillingDateHelper.NextNoticeDate(model);
        }

        public void BindTo(Agreement model)
        {
            Mapper.Map(this, model);
        }

    }
}
