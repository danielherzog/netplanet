﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using netplanet.dal.Entities;
using netplanet.web.Models.Commission;

namespace netplanet.web.Models
{
    [DataContract]
    public class ArticleModel
    {
        [DataMember(Name = "article")]
        public int ArticleId { get; set; }

        [DataMember(Name = "type")]
        public string Type { get; set; }

        [DataMember(Name = "displayType")]
        public string DisplayType { get; set; }

        [DataMember(Name = "amount")]
        public int Amount { get; set; }

        [DataMember(Name = "price")]
        public decimal Price { get; set; }

        [DataMember(Name = "commission")]
        public CommissionModel Commission { get; set; }

        public ArticleModel()
        {

        }

        public ArticleModel(AgreementArticle model)
        {
            Mapper.Map(model, this);
            if (model.CommissionId != null)
            {
                if(model.Commission is PercentCommission)
                { 
                    this.Commission = new PercentCommissionModel((PercentCommission)model.Commission);
                }
                else if (model.Commission is AbsoluteCommission)
                {
                    this.Commission = new AbsoluteCommissionModel((AbsoluteCommission)model.Commission);
                }
            }

            if (model is SingularArticle)
            {
                this.DisplayType = "Einmalig";
                this.Type = "singular";
            }
            else if (model is RecurringArticle)
            {
                this.DisplayType = "Wiederkehrend";
                this.Type = "recurring";
            }
            else
            {
                this.DisplayType = "Volume";
                this.Type = "volume";
            }
        }

        public void BindTo(AgreementArticle model)
        {
            model.ArticleId = this.ArticleId;
            model.Amount = this.Amount;
            model.Price = this.Price;
        }

        public AgreementArticle Get()
        {
            AgreementArticle model = null;

            switch (this.Type)
            {
                case "singular":
                    model = new SingularArticle();
                    break;

                case "recurring":
                    model = new RecurringArticle();
                    break;

                default:
                    model = new VolumeArticle();
                    break;
            }

            model.ArticleId = this.ArticleId;
            model.Amount = this.Amount;
            model.Price = this.Price;

            return model;
        }
    }
}
