﻿using AutoMapper;
using netplanet.dal.Entities;
using netplanet.web.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace netplanet.web.Models
{
    [DataContract]
    public class AgreementListModel
    {
        [DataMember(Name ="id")]
        public int PosId { get; set; }

        [DataMember(Name = "name")]
        public string Name { get; set; }

        [DataMember(Name = "active")]
        public bool Active { get; set; }

        [DataMember(Name = "setupdate")]
        public DateTime SetupDate { get; set; }

        [DataMember(Name = "nextnoticedate")]
        public DateTime NextNoticeDate { get; set; }

        [DataMember(Name = "nextbillingdate")]
        public DateTime BillingDate { get; set; }

        [DataMember(Name = "customer")]
        public int CustomerId { get; set; }


        public AgreementListModel()
        {

        }

        public AgreementListModel(Agreement model)
        {
            Mapper.Map(model, this);
            this.NextNoticeDate = BillingDateHelper.NextNoticeDate(model);
        }
        
    }
}
