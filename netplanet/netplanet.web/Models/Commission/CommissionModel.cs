using System.Runtime.Serialization;
using AutoMapper;

namespace netplanet.web.Models.Commission
{
    [DataContract]
    public class CommissionModel
    {
        [DataMember(Name = "agent")]
        public int Agent { get; set; }

        [DataMember(Name = "type")]
        public string Type { get; set; }


        public CommissionModel()
        {
            
        }

        public CommissionModel(dal.Entities.Commission model)
        {
            Mapper.Map(model, this);
        }
    }
}