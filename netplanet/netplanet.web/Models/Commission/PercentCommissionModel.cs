﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using netplanet.dal.Entities;

namespace netplanet.web.Models.Commission
{
    [DataContract]
    public class PercentCommissionModel : CommissionModel
    {
        [DataMember(Name="value")]
        public int Percent { get; set; }

        public PercentCommissionModel()
        {
            
        }

        public PercentCommissionModel(PercentCommission model) : base(model)
        {
            this.Type = "percent";
        }
    }
}
