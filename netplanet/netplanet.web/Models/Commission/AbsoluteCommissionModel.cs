﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using netplanet.dal.Entities;

namespace netplanet.web.Models.Commission
{
    [DataContract]
    public class AbsoluteCommissionModel : CommissionModel
    {
        [DataMember(Name="value")]
        public decimal Value { get; set; }

        public AbsoluteCommissionModel()
        {
            
        }

        public AbsoluteCommissionModel(AbsoluteCommission model) : base(model)
        {
            this.Type = "absolute";
        }
    }
}
