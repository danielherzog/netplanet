﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Owin;
using Owin;
using System.Data.Entity;
using System.Web.Http;
using Newtonsoft.Json;

[assembly: OwinStartup(typeof(netplanet.web.Startup))]

namespace netplanet.web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            var container = ConfigureDependency(app);

            app.UseCors(Microsoft.Owin.Cors.CorsOptions.AllowAll);

            ConfigureAuth(app, container);
            ConfigureAutomapper(app);

            //Database.SetInitializer(new MyInitializer());

            var jsonFormatter = GlobalConfiguration.Configuration.Formatters.JsonFormatter;
            jsonFormatter.SerializerSettings.DateTimeZoneHandling = DateTimeZoneHandling.Local;

        }
    }
}
