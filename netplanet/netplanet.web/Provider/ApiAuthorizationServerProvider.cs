﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security.OAuth;
using netplanet.dal;
using netplanet.dal.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace netplanet.web.Provider
{
    public class ApiAuthorizationServerProvider : OAuthAuthorizationServerProvider
    {
        private readonly UserManager<ApplicationUser> userManager = null;

        public ApiAuthorizationServerProvider(UserManager<ApplicationUser> userManager)
        {
            this.userManager = userManager;
        }

        public override async Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            context.Validated();
        }

        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {

            var user = await userManager.FindAsync(context.UserName, context.Password);
            if (user == null)
            {
                context.SetError("invalid_grant", "The user name or password is incorrect.");
                return;
            }

            var identity = new ClaimsIdentity(context.Options.AuthenticationType);
            identity.AddClaim(new Claim("sub", context.UserName));
            identity.AddClaim(new Claim("role", "user"));

            user.LastLogin = DateTime.Now;
            await userManager.UpdateAsync(user);

            context.Validated(identity);

        }
    }
}
